import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../common/auth-guard.service';

const routes: Routes = [
  {
    path: 'login_trial',
    loadChildren: './login-trial/login-trial.module#LoginTrialModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
