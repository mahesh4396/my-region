import { Component, OnInit, ViewChild } from '@angular/core';
import { ConstantsService } from '../../common/constants.service';
import { DataFetcherService } from '../../common/data-fetcher.service';
import { ShareManagerService } from '../../common/share-manager.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-login-trial',
  templateUrl: './login-trial.component.html',
  styleUrls: ['./login-trial.component.css']
})
export class LoginTrialComponent implements OnInit {

  btnText: string = "Search";
  clearbtnText: string = "Clear";
  sno = 1;
  itemsPerPage = this._constant.ITEMS_PER_PAGE;
  pages = this._constant.PAGES;
  displayedColumns: string[] = ['#', 'Name', 'User Name', 'Email', 'Mobile'];
  dataSource = new MatTableDataSource<[any]>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  search_data = { "district_id": 0, "constituency_id": 0, "panchayat_id": 0 }
  districts = [];
  constituency = [];
  panchayat = [];
  constructor(private _httpService: DataFetcherService, private _constant: ConstantsService, private sharedData: ShareManagerService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.fetchDistricts();
  }

  districtChange(val) {
    this._httpService.doPostRequest(this._constant.CONSTITUENCYS, { "district_id": val }).subscribe(
      res => {
        if (res.hasOwnProperty('status') && res.status === true) {
          this.constituency = res.data;
        }
      }
    )
  }

  constituencyChange(event) {
    let target = event.source.selected._element.nativeElement;
    var tehsil_id = target.getAttribute('tehsil_id');
    this._httpService.doPostRequest(this._constant.PANCHAYAT, { "tehsil_id": tehsil_id }).subscribe(
      res => {
        if (res.hasOwnProperty('status') && res.status === true) {
          this.panchayat = res.data;
        }
        console.log(this.panchayat);
      }
    )
  }

  searchData() {
    this.fetchData();
  }

  fetchDistricts() {
    this._httpService.doPostRequest(this._constant.DISTRICTS, {}).subscribe(
      res => {
        if (res.hasOwnProperty('status') && res.status === true) {
          this.districts = res.data;
        }
      }
    )
  }

  fetchData() {
    this._httpService.doPostRequest(this._constant.USERS_DETAILS, this.search_data).subscribe(
      res => {
        if (res.hasOwnProperty('status') && res.status === true) {
          var constituency_id = this.search_data.constituency_id;
          let tempArr = [];
          Object.keys(res.data).map(function (obj) {
            Object.keys(res.data[obj]).map(function (con) {
              Object.keys(res.data[obj][con]).map(function (pan) {
                const tempObj = res.data[obj][con][pan];
                if (tempObj instanceof Array) {
                  tempObj.splice(0, 0, { 'isGroup': true, 'group_name': (constituency_id == 0 ? (tempObj[0]['constituency_name'] + ' - ') : '') + tempObj[0]['panchayat_name'] });
                  if (tempArr.length > 0) {
                    tempArr = tempArr.concat(tempObj);
                  } else {
                    tempArr = tempObj;
                  }
                  tempArr.concat(tempObj);
                }
              });
            });
          });
          this.dataSource.data = tempArr;
          if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
          }
        }
      }
    )
  }

  isGroup(index, item): boolean {
    if (item.hasOwnProperty('isGroup')) {
      return true;
    }
    return false;
  }
  generateNo(indx) {
    if (indx === 1) {
      this.sno = 1;
    }
    return this.sno++;
  }

  clearData() {
    this.search_data = { "district_id": 0, "constituency_id": 0, "panchayat_id": 0 }
    this.dataSource.data = [];
  }

  validateUserAddForm() {
    if (this.search_data.district_id === 0) {
      return false;
    }
    return true;
  }

}
