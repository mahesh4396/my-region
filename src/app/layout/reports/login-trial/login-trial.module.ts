import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../material';
import { LoginTrialRoutingModule } from './login-trial-routing.module';
import { LoginTrialComponent } from './login-trial.component';

@NgModule({
  declarations: [LoginTrialComponent],
  imports: [
    CommonModule,
    LoginTrialRoutingModule,
    MaterialModule
  ]
})
export class LoginTrialModule { }
