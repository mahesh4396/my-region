import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginTrialComponent } from './login-trial.component';

const routes: Routes = [
  {
    path: '',
    component: LoginTrialComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginTrialRoutingModule { }
