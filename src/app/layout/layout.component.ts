import { Component, OnInit } from '@angular/core';
import { ConstantsService } from './common/constants.service';
import { ShareManagerService } from './common/share-manager.service';
import { DataFetcherService } from './common/data-fetcher.service';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener
} from '@angular/material/tree';
import { of as observableOf } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Router } from '@angular/router';

export class MenuData {
  children: MenuData[];
  menu: string;
  category: string;
  link: string;
  menu_type: number;
}
export interface FileNode {
  name: string;
  link: string;
  children?: FileNode[];
}

export interface FlatTreeNode {
  name: string;
  link: string;
  level: number;
  expandable: boolean;
}

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  menuLinks = [];
  userInfo = {};
  selectedData = {};
  selectedServiceId = 0;
  apart_details = {};
  selectedCategoryId = 0;
  selectedPrivilegeId = 0;

  treeControl: FlatTreeControl<FlatTreeNode>;

  /** The TreeFlattener is used to generate the flat list of items from hierarchical data. */
  treeFlattener: MatTreeFlattener<FileNode, FlatTreeNode>;

  /** The MatTreeFlatDataSource connects the control and flattener to provide data. */
  dataSource: MatTreeFlatDataSource<FileNode, FlatTreeNode>;

  constructor(
    private _constants: ConstantsService,
    private _httpservice: DataFetcherService,
    private sharedData: ShareManagerService,
    private route: Router
  ) { 
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this.getLevel,
      this.isExpandable,
      this.getChildren
    );

    this.treeControl = new FlatTreeControl(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(
      this.treeControl,
      this.treeFlattener
    );
  }

  /** Transform the data to something the tree can read. */
  transformer(node: FileNode, level: number) {
    return {
      name: node.name,
      link: node.link,
      level: level,
      expandable: !!node.children
    };
  }

  /** Get the level of the node */
  getLevel(node: FlatTreeNode) {
    return node.level;
  }

  /** Get whether the node is expanded or not. */
  isExpandable(node: FlatTreeNode) {
    return node.expandable;
  }

  /** Get whether the node has children or not. */
  hasChild(index: number, node: FlatTreeNode) {
    return node.expandable;
  }

  /** Get the children for the node. */
  getChildren(node: FileNode) {
    return observableOf(node.children);
  }

  ngOnInit() {
  }

  doLogout() {
    this.sharedData.doLogout();
  }

}
