import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ShareManagerService } from './share-manager.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    private shareManager: ShareManagerService
    constructor() {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                console.log("Test")
                // auto logout if 401 response returned from api
                this.shareManager.doLogout();
                console.log("Sample")
            }
            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }
}
