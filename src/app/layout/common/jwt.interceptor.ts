import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ShareManagerService } from './share-manager.service';
import { ConstantsService } from './constants.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private shareManager: ShareManagerService, private _constants: ConstantsService, private jwtHelper: JwtHelperService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        console.log("asdasd" + this._constants.SKIP_TOKEN.indexOf(request.url))
        let token = "";
        if (this._constants.SKIP_TOKEN.indexOf(request.url) < 0) {
            const tokenData = this.shareManager.getDataFromLocalStorage();
            token = tokenData['token'];
        }
        request = request.clone({
            setHeaders: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4=',
                'responseType': 'json',
                'x-access-token': token
            }
        });
        return next.handle(request);
    }
}
