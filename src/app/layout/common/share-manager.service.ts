import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { JwtHelperService } from '@auth0/angular-jwt';
import { ConstantsService } from '../common/constants.service';
import * as crypto from 'crypto-js';
@Injectable({
  providedIn: 'root'
})
export class ShareManagerService {

  constructor(private router: Router, private constants: ConstantsService, private jwtHelper: JwtHelperService) { }

  addStorage(token_data) {
    const encrypted = crypto.AES.encrypt(JSON.stringify(token_data), this.constants.AES_ENC_KEY);
    localStorage.setItem(this.constants.LBDB_KEY, encrypted);
  }
  updateLocalStorage() {
    const data = localStorage.getItem(this.constants.LBDB_KEY);
    if (data.length > 0) {
      const encrypted = crypto.AES.encrypt(data, this.constants.AES_ENC_KEY);
      localStorage.setItem(this.constants.LBDB_KEY, encrypted);
    } else {
      this.clearLocalStorage();
    }
  }
  getDataFromLocalStorage() {
    const encryptedData = localStorage.getItem(this.constants.LBDB_KEY);
    let object;
    if (encryptedData != null && encryptedData.length > 0) {
      if (encryptedData.indexOf('{') > -1) {
        object = JSON.parse(encryptedData);
      } else {
        const decrypted = crypto.AES.decrypt(encryptedData, this.constants.AES_ENC_KEY);
        object = JSON.parse(decrypted.toString(crypto.enc.Utf8));
      }
    } else {
      this.clearLocalStorage();
    }
    return object;
  }
  getJWTToken() {
    const token_data = this.getDataFromLocalStorage();
    if (token_data !== undefined && token_data.hasOwnProperty('token')) {
      let token = token_data['token'];
      return token;
    }
    return token_data;
  }
  getDataFromJWTToken() {
    const helper = new JwtHelperService();
    const token_data = this.getDataFromLocalStorage();
    let decodedToken;
    if (token_data.hasOwnProperty('token')) {
      let token = token_data['token'];
      const tokenSplit = token.split('.');
      decodedToken = helper.decodeToken(token);
    }
    return decodedToken;
  }
  isValidToken() {
    const token = this.getJWTToken();
    if (token !== undefined && token.length > 0 && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    return false;
  }
  clearLocalStorage() {
    localStorage.removeItem(this.constants.LBDB_KEY);
    this.router.navigate([this.constants.LOGOUT_REDIRCT_LINK]);
  }
  addTemparoryDataStorage(key, data) {
    const encrypted = crypto.AES.encrypt(JSON.stringify(data), this.constants.AES_ENC_KEY);
    localStorage.setItem(key, encrypted);
  }
  doLogout() {
    this.clearLocalStorage();
  }
}

