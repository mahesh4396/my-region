import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ShareManagerService } from '../common/share-manager.service';
import { ConstantsService } from '../common/constants.service';
@Injectable({
  providedIn: 'root'
})
export class DataFetcherService {
  constructor(private _http: HttpClient, private shareData: ShareManagerService, private constants: ConstantsService) { }
  base_path = this.constants.BASE_NODE_URL;
  updateAuthToken = 0;
  doGetRequest(api_path) {
    const url = this.base_path + api_path;
    return this._http.get<any>(url, this.getHeaders(url,0));
  }
  doPostRequest(api_path, body) {
    const url = this.base_path + api_path;
    return this._http.post<any>(url, body, this.getHeaders(url,0));
  }
  doPutRequest(api_path, body) {
    const url = this.base_path + api_path;
    return this._http.put<any>(url, body, this.getHeaders(url,0));
  }
  doDeleteRequest(api_path) {
    const url = this.base_path + api_path;
    return this._http.delete<any>(this.base_path + api_path, this.getHeaders(url,0));
  }
  doFileUpload(api_path,body){
    const url = this.base_path + api_path;
    return this._http.post<any>(url, body, this.getHeaders(url,1));
  }
  getHeaders(url,flag) {
    let token = '';
    if (this.constants.SKIP_TOKEN.indexOf(url) < 0) {
      const tokenData = this.shareData.getDataFromLocalStorage();
      token = tokenData['token'];
    }
    if(flag == 0){
      return {'headers': new HttpHeaders ({ 'Content-Type': 'application/json',
    'Authorization': 'Basic YWRtaW46YWRtaW4=', 'responseType': 'json' , 'x-access-token': token})};
    }else{
      return {'headers': new HttpHeaders ({'Authorization': 'Basic YWRtaW46YWRtaW4=', 'responseType': 'json' , 'x-access-token': token})};
    }
  }
  refreshAuthToken() {
    const tokenData = this.shareData.getDataFromLocalStorage();
    this.doPostRequest(this.constants.REF_TOKEN_URL, { 'refreshToken': tokenData['refreshToken'] }).subscribe(res => {
      if (res.status === true) {
        res.refToken = tokenData['refreshToken'];
        this.shareData.addStorage({ token: res.token, refreshToken: tokenData['refreshToken'] });
      }
    });
  }
}
