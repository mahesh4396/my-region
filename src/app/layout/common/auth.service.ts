import { Injectable } from '@angular/core';
import { ShareManagerService } from './share-manager.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private shareManager: ShareManagerService) { }
  public isAuthenticated(): boolean {
    return this.shareManager.isValidToken();
  }
}
