import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  AES_ENC_KEY = 'aes_enc_key';
  SERVICE_ID = 1;
  LBDB_KEY = 'myregion_data';
  PAGE_COUNT = 20;
  BASE_NODE_URL = 'http://localhost:3000/';
  LOGOUT_REDIRCT_LINK = 'login';
  LOGIN_URL = 'user/login';
  REGISTER_URL = 'user/regUsers';
  INSTANT_TOKEN = 'auth/instant_token';
  TOKEN_URL = 'auth/auth_token';
  REF_TOKEN_URL = 'auth/refresh_token';
  DISTRICTS = 'common/districts';
  CONSTITUENCYS = 'common/constituency';
  PANCHAYAT = 'common/panchayat';
  USERS_DETAILS = 'user/getUsers';


  SKIP_TOKEN = [this.BASE_NODE_URL + this.INSTANT_TOKEN];
  CHECK_ROLES = [3, 4];
  ITEMS_PER_PAGE = 10;
  PAGES = [5, 10, 25, 50, 100,500,1000];

  // error messages
  INVALID_MSG = 'You must enter a value';
  constructor() { }
  // build script
  // ng build --base-href /ams/ --prod --aot --build-optimizer
}
