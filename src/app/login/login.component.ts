import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { DataFetcherService } from '../layout/common/data-fetcher.service';
import { ConstantsService } from '../layout/common/constants.service';
import { ShareManagerService } from '../layout/common/share-manager.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login_params = { 'uname': '', 'upwd': '' };
  forgotpwd_params = { 'uname': '', 'upwd': '' };
  displayMsg = {}
  constructor(private renderer: Renderer2, private router: Router, private _constants: ConstantsService,
    private sharedManager: ShareManagerService, private _httpservice: DataFetcherService) { }

  ngOnInit() {
  }

  loginBtnClicked() {
    if (this.login_params.uname.length > 0 && this.login_params.upwd.length > 0) {
      this.generateToken(1);
    }
  }
  loginServiceCall() {
    this._httpservice.doPostRequest(this._constants.LOGIN_URL, this.login_params).subscribe(res => {
      if (res.hasOwnProperty('status') && res.status === true) {
        this.sharedManager.addStorage(res);
        this.router.navigate(['/']);
      }
      this.displayMsg = res;
    });
  }
  generateToken(serviceType) {
    this._httpservice.doPostRequest(this._constants.INSTANT_TOKEN, { 'host': '', 'ip': '' }).subscribe(res => {
      if (res.hasOwnProperty('status') && res.status === true) {
        this.sharedManager.addStorage(res);
        if (serviceType === 1) {
          this.loginServiceCall();
        }
      }
    });
  }
}
