import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material/select';
import {
    MatGridListModule, MatSidenavModule, MatPaginatorModule, MatSortModule, MatButtonModule,
    MatListModule, MatMenuModule, MatIconModule, MatRadioModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatTreeModule } from '@angular/material/tree';
import { MatTableModule } from '@angular/material/table';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';

@NgModule({
    imports: [CommonModule, MatTableModule, MatPaginatorModule, MatSortModule,
        MatTreeModule, MatSidenavModule, MatListModule, ReactiveFormsModule, FormsModule, LayoutModule,
        MatGridListModule, MatMenuModule, MatIconModule, MatRadioModule, MatSelectModule, MatButtonModule,
        MatCardModule, MatToolbarModule, MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatDialogModule, MatCheckboxModule, MatDatepickerModule],
    exports: [CommonModule, MatTableModule, MatPaginatorModule, MatSortModule, MatTreeModule,
        MatSidenavModule, MatListModule, ReactiveFormsModule, FormsModule, LayoutModule, MatGridListModule,
        MatMenuModule, MatIconModule, MatRadioModule, MatSelectModule, MatButtonModule, MatCardModule, MatToolbarModule,
        MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatDialogModule, MatCheckboxModule, MatDatepickerModule]
})
export class MaterialModule { }
